import { Streamlit } from "streamlit-component-lib"

function onRender(event) {
  const data = event.detail
  window.seqviz
    .Viewer("root", {
      name: data.args["name"],
      seq: data.args["seq"],
      style: data.args["style"],
      annotations: data.args["annotations"],
      highlights: data.args["highlights"],
      enzymes: data.args["enzymes"]
    })
    .render();
  Streamlit.setFrameHeight()
}

Streamlit.events.addEventListener(Streamlit.RENDER_EVENT, onRender)
Streamlit.setComponentReady()
Streamlit.setFrameHeight()